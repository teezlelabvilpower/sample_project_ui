var _ = require("underscore");

var  page;
var homeLayout = "layout/home-layout.html";
var notificationJson = require("../object/notification.json");
var historyData = require('../object/historyGridData.json');
var statusGridData = require('../object/statusGridData.json');
var rawMessage = require('../object/rawMessage.json');
var decodeMessage = require('../object/decodeMessage.json');
var alarmMessage = require('../object/alarm.json');
var dailyReport = require('../object/dailyReport.json');
var accountList = require('../object/accountList.json');
var assetList = require('../object/assetList.json');
var snaplist=require('../object/snap.json');
var tankData=require('../object/manageTank.json');
var userData=require('../object/manageUser.json');
var mapData=require('../object/map.json');


var UIAction = function (app) {
    this.app = app;
    this.conf = app.conf;
};

module.exports = UIAction;

// Index page
UIAction.prototype.getIndexPage = function(req,res){

    var reqUrl = req.headers.host;
    /*res.render("home/home.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl});*/
    res.redirect('/status');
};
UIAction.prototype.getLoginPage = function(req,res){
    page = 'login';

 var reqUrl = req.headers.host;
    res.render("login.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl , page : page });

};
UIAction.prototype.getLoginSubmitPage = function(req,res) {

    var reqUrl = req.headers.host;
    console.log(req.query.email);
    if (req.query.email == 'tzuser' && req.query.pwd == '2weeks') {
        res.redirect("/status")
    }
    else
    {
        res.redirect("/login")
    }



};

UIAction.prototype.getStatusPage = function(req,res){
    page = 'status';

    var   temperature = 'F';
    var reqUrl = req.headers.host;
    res.render("monitoring/status.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl , page : page , accounts : accountList , assets : assetList });

};


UIAction.prototype.getSnapshotPage = function(req,res){

    page = 'snapshot';
    var reqUrl = req.headers.host;
    res.render("monitoring/snapshot.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl , page : page, accounts : snaplist  });

};


UIAction.prototype.getMappingPage = function(req,res){

    page = 'mapping';
    var reqUrl = req.headers.host;
    res.render("monitoring/mapping.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl ,page : page,accounts : accountList,assets : assetList});

};

UIAction.prototype.getReportPage = function(req,res){

    page = 'report';
    var reqUrl = req.headers.host;
    res.render("monitoring/report.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl ,page : page , accounts : accountList });

};

UIAction.prototype.getNotifyPage = function(req,res){
  
    page = 'notification';
    var reqUrl = req.headers.host;
    res.render("monitoring/notification.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl ,page : page ,data :notificationJson});

};

UIAction.prototype.getMyAccountPage = function(req,res){

    page = 'myAccount';
    var reqUrl = req.headers.host;
    res.render("administration/myaccount.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl ,page : page });

};



UIAction.prototype.getManageAccountPage = function(req,res){

    page = 'manageAccount';
    var reqUrl = req.headers.host;
    res.render("administration/manageAccount.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl ,page : page });

};
UIAction.prototype.getManageTankPage = function(req,res){

    page = 'manageAccount';
    var reqUrl = req.headers.host;
    res.render("administration/manageTank.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl ,page : page});

};

UIAction.prototype.getManageAccountMenuPage = function(req,res){

    page = 'manageAccount';
    var reqUrl = req.headers.host;
    res.render("administration/accounts.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl ,page : page });

};



UIAction.prototype.getManageUserPage = function(req,res){

    page = 'manageAccount';
    var reqUrl = req.headers.host;
    res.render("administration/manageUsers.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl ,page : page, });

};



UIAction.prototype.getTankiq2 = function(req,res){

    page = 'report';
    var reqUrl = req.headers.host;
    res.render("home/tankIq2_0.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl ,page : page });

};

UIAction.prototype.getmanageDevice = function(req,res){

    page = 'manageAccount';
    var reqUrl = req.headers.host;
    res.render("administration/manageDevice.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl ,page : page });

};

UIAction.prototype.getmyaccount = function(req,res){

    page = 'manageAccount';
    var reqUrl = req.headers.host;
    res.render("administration/myaccount.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl ,page : page });

};


/*UIAction.prototype.getGridPage = function(req,res){

    var reqUrl = req.headers.host;
    res.render("sample-grid.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl});

};*/

/*statusPage data apiCall */

UIAction.prototype.getStatusGridData = function(req,res){
    var reqUrl = req.headers.host;
    res.send(statusGridData);

};
UIAction.prototype.getaccountListData = function(req,res){
    var reqUrl = req.headers.host;
    res.send(accountList);

};
UIAction.prototype.gettankListData = function(req,res){
    var reqUrl = req.headers.host;
    res.send(assetList);

};

UIAction.prototype.getrawMessageData = function(req,res){
    var reqUrl = req.headers.host;
    res.send(rawMessage);

};

UIAction.prototype.getdecodeData = function(req,res){
    var reqUrl = req.headers.host;
    res.send(decodeMessage);

};


UIAction.prototype.getalarmData = function(req,res){
    var reqUrl = req.headers.host;
    res.send(alarmMessage);

};


UIAction.prototype.getdailyData = function(req,res){
    var reqUrl = req.headers.host;
    res.send(dailyReport);

};

UIAction.prototype.getHistoryData = function(req,res){
    var reqUrl = req.headers.host;
    res.send(historyData);
};

UIAction.prototype.getManageTankData = function(req,res){
    var reqUrl = req.headers.host;
    res.send(tankData);
};
UIAction.prototype.getManageUserData = function(req,res){
    var reqUrl = req.headers.host;
    res.send(userData);
};
UIAction.prototype.getMapTable = function(req,res){
    var reqUrl = req.headers.host;
    res.send(mapData);
};