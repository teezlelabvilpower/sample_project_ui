/**
 * Created by packyag on 13/12/17.
 */

function tzAjax(url, method, data, successCallback, errorCallback, config) {

    data = data || {};

    $.ajax({
        url: url,
        method: method,
        data: data,
        beforeSend: function (request) {

            //request.setRequestHeader("Content-Type","text/plain");
            //request.setRequestHeader("Content-Type","application/json");
        },
        //dataType: "json",
        success: function (result) {
            if (successCallback && typeof successCallback == "function") {
                successCallback(result);
                tzAjaxSuccessHandler(url, method, data, result);
            }
            else {
                console.log("Error in Ajax success callback")
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            if (errorCallback && typeof errorCallback == "function") {
                errorCallback(xhr, textStatus, errorThrown);
                tzAjaxErrorHandler(xhr, textStatus, errorThrown);
            }
            else {
                console.log("Error in Ajax success callback")
            }
            console.log("Err", xhr);
        }
    });
}
