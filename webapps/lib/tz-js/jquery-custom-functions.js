/**
 * Created by packyag on 13/12/17.
 */
$.extend({
    getUrlVars: function(){
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getUrlVar: function(name){
        return $.getUrlVars()[name];
    },
    getUrlVarDecoded: function(name){
        var r = $.getUrlVars()[name];
        return !r ? '' : decodeURIComponent(r.replace(/\+/g, " "));
    },
    numbersOnly : function (e) {
        if(!e){
            e = window.event;
        }
        // Allow: backspace, delete, tab, escape, enter and Dot is not allowed(110, 190)
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    },
    forceNumbersOnly:function () {
        /* return this.each(function()
         {
         $(this).keydown(function(e)
         {
         var key = e.charCode || e.keyCode || 0;
         // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
         // home, end, period, and numpad decimal
         return (
         key === 8 ||
         key === 9 ||
         key === 13 ||
         key === 46 ||
         key === 110 ||
         key === 190 ||
         (key >= 35 && key <= 40) ||
         (key >= 48 && key <= 57) ||
         (key >= 96 && key <= 105));
         });
         });*/
    }
});

function handleNumericInput(e) {

    if(!e){
        e = window.event;
    }
    // Allow: backspace, delete, tab, escape, enter and Dot is not allowed(110, 190)
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
            // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return true;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
        return false;

    }

    return true;
}

$(document).on('keydown', '.numbers-only', function (e) {

    var c  = handleNumericInput(e);

    if(!c){
        var $p = $(this).parent();

        if($p.hasClass('form-group') && !$p.hasClass('has-error')){
            $p.addClass('has-error');
            setTimeout(function () {
                $p.removeClass('has-error');
            },200)
        }
        return false;
    }

    return c;

});
