/**
 * Created by packyag on 13/12/17.
 */

const _tzHttpErrMsg = {
      0 : 'Status code: 0',
    400 : 'Bad Request',
    401 : 'Unauthorized',
    403 : 'Forbidden',
    404 : 'Not Found',
    408 : 'Request Timeout',
    429 : 'Too Many Requests',
    500 : 'Internal Server Error',
    502 : 'Bad Gateway',
    503 : 'Service Unavailable',
    504 : 'Gateway Timeout'
};

function tzAjaxSuccessHandler(url,method,data,result) {
    try {
        if (result && result.status && (result.status == 'ERROR' || result.status == 'Error')) {
            console.warn('\tError while getting data. \n\tURL:', url, '\n\tMETHOD:', method, '\n\DATA:', data, '\n\RESULT:', result);

            if(result.error && result.error.code && (result.error.code === '109')){

                window.location.href = '/login';

            }
            /*if (tzlTrack) {
                var d = JSON.stringify(data), e = JSON.stringify(result);
                tzlTrack.trackThis('Error: Error response from server in common ajax', {error: {url: url, method: method, data: d, error: e}}, true)
            } else {
                console.warn('MixPanel not found. Error not send to MixPanel.')
            }*/
        }
    } catch (e) {
        console.log("Error in tzSuccessHandler",e);
    }
}

function tzAjaxErrorHandler(xhr, textStatus, errorThrown) {
    try {
        console.warn('\tError in Ajax. \n\tError Code:', xhr.status, '\n\tError Message:', (_tzHttpErrMsg[xhr.status] || 'Error Message Not Defined'));
        /*if (tzlTrack) {
            tzlTrack.trackThis('Ajax Error', {error: {Status: xhr.status, Message: _tzHttpErrMsg[xhr.status]}}, true)
         } else {
            console.warn('MixPanel not found. Error not send to MixPanel.')
         }*/
    } catch (e) {
        console.log("Error in tzErrorHandler",e);
    }
}