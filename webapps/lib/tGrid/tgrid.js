// tGrid Basic v1.6
$(document).ready(function () {
    _.templateSettings = {
        evaluate: /\{\%(.+?)\%\}/g,
        interpolate: /\{\%=(.+?)\%\}/g,
        escape : /{%-([\s\S]+?)%}/g
    };
});
var tGrid = function (tableObj, callback) {
    var self = this;
    self.thead = tableObj.table_header;
    self.url = tableObj.url ? tableObj.url : null;
    self.params = tableObj.params;
    self.async = tableObj.async || true;
    self.sortByList = tableObj.params.sort || [];
    self.sortType = tableObj.sortType || 'multiple';
    self.templateId = tableObj.templateId;
    self.tbody = tableObj.table_body;
    self.tclass = tableObj.table_class || 'table-bordered table-condensed';
    self.gridtype = 0;
    self.data = tableObj.data;
    self.targetId = tableObj.targetId;
    self.recperpage = tableObj.recperpage ? tableObj.recperpage : [10, 25, 50, 100];
    self.isMoreEnabled = tableObj.isMoreEnabled ? tableObj.isMoreEnabled : false;
    self.moreLink = tableObj.moreLink ? tableObj.moreLink : 'Load More';
    self.hideFooter = tableObj.hideFooter ? tableObj.hideFooter : false;
    self.layout = tableObj.layout ? tableObj.layout : 'div';
    self.clickType = [11]; //[00,01,10,11];
    self.loading = ((tableObj.loading != '') ? tableObj.loading : 'Loading data') || 'Fetching data';
    self.loadingDots = ((tableObj.loadingDots != '') ? tableObj.loadingDots : '...') || '<span>.</span><span>.</span><span>.</span>';
    self.scrollId = tableObj.scrollId ? tableObj.scrollId : 'body';
    self.autoScroll = true;

    /* New */
    self.refreshId = tableObj.refreshId;
    self.nonFixedDrop = tableObj.nonFixedDrop || false;
    self.scrollableParent = tableObj.scrollableParent || null;
    self.prevDOM = tableObj.prevDOM;
    self.nextDOM = tableObj.nextDOM;
    self.stickyHeader = tableObj.stickyHeader || false;
    self.searchOptions = !_.isUndefined(tableObj.searchOptions) ? (!_.isEmpty(tableObj.searchOptions) ? tableObj.searchOptions : {}) : null;
    self.populate(self, tableObj, callback);

    return self;

};

tGrid.prototype = {

    populate: function (self, tableObj, callback, type) {

        if (self.url) {
            $.ajax({
                type: 'GET',
                url: self.url,
                data: self.params,
                async: self.async,
                success: function (data) {

                    if(_.isUndefined(self.templateId) && _.isUndefined(self.thead) && _.isUndefined(self.tbody)){
                        alert('Template ID not mentioned in tGrid object');return false;
                    }
                    if(!_.isUndefined(self.templateId)){
                        self.gridtype = 2;
                        var _tableTmp = $(self.templateId).html();
                        if (_.isUndefined(_tableTmp) || !_tableTmp) {
                            console.info('TGrid template "'+ self.templateId +'" not found. Operation cancelled.');
                            return false;
                        }
                        var _headTmp = _tableTmp.slice(_tableTmp.search("<tgrid:head>"),_tableTmp.search("</tgrid:head>"));
                        var _bodyTmp = _tableTmp.slice(_tableTmp.search("<tgrid:body>"),_tableTmp.search("</tgrid:body>"));
                        self.thead = _headTmp.replace('<tgrid:head>','');
                        self.tbody = _bodyTmp.replace('<tgrid:body>','');
                        //var location = self.tbody.slice(self.tbody.search("<tgrid:location>"),self.tbody.search("</tgrid:location>"))
                        //$(self.tbody)
                    }
                    if((!_.isUndefined(self.thead) && !_.isUndefined(self.tbody))){
                        self.gridtype = 1
                    }

                    self.data = data;
                    tableObj.data = data
                    if (self.layout == 'table') {
                        self.drawTGrid(self, tableObj, data, callback, type);

                    } else {
                        self.drawDiv(self, tableObj, data, callback);
                    }

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $(self.targetId).html('<h5 style="color: #999; font-weight: 500;"><center>System error. Please contact administrator</center></h5>');
                }
//                timeout : 60000
            });
        } else {
            if (self.layout == 'table') {
                self.drawTGrid(self, tableObj, self.data, callback);
            } else {
                self.drawDiv(self, tableObj, self.data, callback);
            }
        }
    },
    drawTGrid: function (self, tableObj, data, callback, type) {

        var table = self;


        if((table.isMoreEnabled && !$(table.targetId + ' tbody').exists()) || (!table.isMoreEnabled || !$(table.targetId + ' tbody').exists())){
            $(table.targetId).html('<div class="row"><div class="col-lg-12 tgrid-content table-responsive"><table style="margin-bottom:10px;" class="table ' + table.tclass + '"><thead></thead><tbody></tbody></table></div></div><div class="row"><div class="col-lg-12 tgrid-footer" style="margin-bottom: 10px; margin-top: 15px;"></div></div>'); //<tfoot></tfoot> - removed
        }

        if(_.contains(table.clickType, type) || self.params.offset == 0){
            $(table.targetId).html('<div class="row"><div class="col-lg-12 tgrid-content table-responsive"><table style="margin-bottom:10px;" class="table ' + table.tclass + '"><thead></thead><tbody></tbody></table></div></div><div class="row"><div class="col-lg-12 tgrid-footer" style="margin-bottom: 10px; margin-top: 15px;"></div></div>'); // <tfoot></tfoot> - removed
        }

        // header
        $(table.targetId + ' thead').html(table.thead);

        var tableHeaderObj = $(table.targetId).find('thead');
        var headerSpan = tableHeaderObj.find('th').length ? tableHeaderObj.find('th').length : 20;

        if(!tableHeaderObj.find('th').exists())
            headerSpan = tableHeaderObj.find('td').length ? tableHeaderObj.find('td').length : 20;


        if (table.sortByList) {
            _.each(table.sortByList, function (num, key) {
                var $gridSortDOM = $(table.targetId).find("[data-sort='" + num.columnName + "']");
                $gridSortDOM.append('<span>');
                if (num.order == -1) {
                    $gridSortDOM.addClass('desc')
                } else if (num.order == 1) {
                    $gridSortDOM.addClass('asc')
                } else {
                    $gridSortDOM.removeClass('asc').removeClass('desc')
                }
            });
        }

        // body content main
        if (data.data.list && data.data.list.length > 0) {

            _.each(data.data.list, function (val, key) {
                val['_index'] = key;
                $(table.targetId + ' tbody').append(_.template(table.tbody, {variable: 'data'})(val));
            });
            self.footerTarget = $(table.targetId + ' .tgrid-footer');
            this.drawPagination(self, tableObj, data, callback, type);

        } else {
            //$(table.targetId + ' tbody').html('<tr><td colspan="' + headerSpan + '" style="text-align: center; height:150px; vertical-align:middle;" class="tfooter"><h3 style="color:#BBB">No data found</h3></td></tr>');

            // by vish for jl-aa-portal
            $(self.targetId).html('<h3 style="color:#BBB;font-style: italic;vertical-align: middle"><center>No data found</center></h3>');

        }
        //Sticky Header
        //if(table.stickyHeader){
        //$(table.targetId + ' table').stickyTableHeaders({ scrollableArea: table.targetId + ' .tgrid-content' });
        //}

        try {

            //Added by vishnu
            $(table.targetId).trigger('tgrid-loaded',{targetId : table.targetId});

            callback(table);

        } catch (e) {}


    },
    drawPagination : function(self, tableObj, data, callback, type){

        var table = self;
        //record per page
        var rpPage = (data.data.limit  ? data.data.limit : 10) || 10;

        var tRec = self.data.data.count;
        var tPage = Math.round(Math.ceil(tRec / rpPage));
        var currentPage = Math.round(self.data.data.offset / self.data.data.limit)+1;

        if(!table.hideFooter){
            if(!table.isMoreEnabled){
                $(table.footerTarget).html('<div style="text-align: right" class="form-inline tfooter">' +
                '<div class="btn-group dropup records-per-page pull-right" style="margin-right: 10px; display: inline-block;">' +
                '<button class="btn btn-sm btn-default records-per-page-value">' + rpPage + '</button>' +
                '<button data-toggle="dropdown" class="btn btn-sm btn-default dropdown-toggle" style="">' +
                '<span class="caret"></span>' +
                '</button>' +
                '<ul style="min-width: 50px" class="dropdown-menu records-per-page-options" title="Records per page">');

                // Records per page
                $.map(table.recperpage, function (val, j) {
                    if (val) {
                        var class_li = "rec rec" + val;
                        if (val == rpPage) {
                            class_li = "active rec rec" + val;
                        }
                        $(table.targetId + ' .tfooter .records-per-page-options').append('<li class="' + class_li + '"><a href="javascript:void(0);">' + val + '</a></li>');
                    }
                });

                table.footerTarget.find('.tfooter').prepend('<div class="paginationHolder pull-right" style="padding-right: 30px">');
                var selectionBox = '<select class="currentPageSelection form-control" style="width:40px; display: inline-block; margin-bottom: 0px; font-size: 11px; height: 30px; padding: 0px;">';

                if(tPage < 500){
                    for (var j = 1; j <= tPage ; j++) {
                        selectionBox += '<option value="' + j + '">' + j + '</option>';
                    }
                    selectionBox += '</select>';
                }else{
                    selectionBox = '<input class="currentPageSelection form-control" type="text" style="margin-bottom:0px; width:50px;" value="'+ currentPage +'">';
                }
                table.footerTarget.find('.paginationHolder').append('<a class="btn prev btn-sm btn-default dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"> &laquo; </a>');
                table.footerTarget.find('.paginationHolder').append('&nbsp;&nbsp; Page ' + selectionBox);
                table.footerTarget.find('.paginationHolder').append(' out of <b class="totalPageCount">' + tPage + '</b> &nbsp;&nbsp;');
                table.footerTarget.find('.paginationHolder').append('<a class="btn next btn-sm btn-default dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">&raquo;</a>');

                table.footerTarget.find('.currentPageSelection').val(currentPage);

                if (tPage == currentPage) {
                    table.footerTarget.find('.next').removeClass('btn-primary');
                } else {
                    table.footerTarget.find('.next').addClass('btn-primary');
                }
                if (currentPage == 1) {
                    table.footerTarget.find('.prev').removeClass('btn-primary');
                } else {
                    table.footerTarget.find('.prev').addClass('btn-primary');
                }

            }else { // more enabled
                if (tPage != currentPage) {
                    $(table.footerTarget).html('<center><a style="cursor:pointer;" class="tgrid-more">'+ table.moreLink +'...</a></center>');
                }else{
                    $(table.footerTarget).html('');
                }
            }
        }
        this.bindEvents(table, tRec, tPage, currentPage, tableObj, callback);
    },
    bindEvents : function(table, tRec, tPage, currentPage, tableObj, callback){

        console.info("Is fixed Drop enabled: "+table.nonFixedDrop);
        if(table.nonFixedDrop){
            $('.tgrid-content').unbind().on('shown.bs.dropdown', function (e) {
                var t = $(this),
                    m = $(e.target).find('.dropdown-menu'),
                    tb = t.offset().top + t.height(),
                    mb = m.offset().top + m.outerHeight(true),
                    d = 20; // Space for shadow + scrollbar.
                if (t[0].scrollWidth > t.innerWidth()) {
                    if (mb + d > tb) {
                        t.css('padding-bottom', ((mb + d) - tb));
                    }
                } else {
                    t.css('overflow', 'visible');
                }
            }).on('hidden.bs.dropdown', function () {
                $(this).css({'padding-bottom': ''});
            });
        }else{
            var dropWidth = null;
            function dropDownFixPosition(button, dropDown){
                if(!button || !dropDown){
                    console.info("Fixed DOM not found");
                    return;
                }
                var dropDownTop = (button.offset().top - $(window).scrollTop()) + button.outerHeight(),
                    dropDownLeft = (button.offset().left - $(window).scrollLeft()),
                    bW = button.width(),
                    bH = button.height(),
                    dW = dropDown.data('max-drop-width'),
                    isWidthAltered = dropDown.hasClass('width_altered');

                dropDown.css('position', 'fixed');
                dropDown.css('z-index', 999);
                dropDown.css('left', (dropDownLeft + bW - dW) + "px");
                dropDown.css('top', dropDownTop + "px");

                if(dW && !isWidthAltered) {
                    //dropWidth = dW;
                    dropDown.css('width', (dW + 5) + "px");
                    dropDown.addClass('width_altered')
                }

                var winHt = $(window).height(),
                    fooHt = $('#home-footer').outerHeight(),
                    dH = button.parent().find('.dropdown-menu').height();

                //console.info(dropDownTop, dropDownLeft, bW, winHt,dH, dW);
                if(winHt < (dropDownTop + dH + fooHt)){
                    dropDown.css({
                        'overflow-y': 'auto',
                        'max-height' : '140px'
                    });
                    var dh = dropDown.height();
                    //TODO: To make the drop down open at the top.
                    if((winHt - fooHt) < (dropDownTop + dh)) {
                        dropDown.css('top', (dropDownTop - (dh + bH + 15)) + "px");
                    }
                }
            }

            $('.tgrid-content').unbind().on('shown.bs.dropdown', function(e) {
                var t = $(e.target).find('a[data-toggle="dropdown"]'),
                    d = $(e.target).find('.dropdown-menu');
                d.addClass('fixed_drop');

                var maxWidth = Math.max.apply(null, $(d).find('li').map( function () {
                    return $(this).find('a').outerWidth( true );
                }).get());

                d.data('max-drop-width', maxWidth);
                console.info('Tgrid action dropdown max-width: '+maxWidth+'px');
                dropDownFixPosition(t, d);
                $(table.scrollableParent || document).on('scroll', function(evt){
                    dropDownFixPosition(t, d);
                });
                $('.tgrid-content').on('scroll', function(evt){
                    dropDownFixPosition(t, d);
                });

            });
        }

        // Search Box Implementation
        /* Search - Default  Options.
         * searchOptions = {
         *   "enabled" : false,
         *   "inputId" : '#TextBoxId',
         *   "inputClearId" : '#InputClearButtonId',
         *   "filterId" : '#FilterId',
         *   "searchId" : '#SearchButtonId',
         *   "autoChange" : true,
         *   "enterSearch" : false,
         * }
         * */

        if(table.searchOptions && table.searchOptions.enabled){
            console.info("T-Grid auto search enabled");
            var options = table.searchOptions,
                filterId = options.filterId || null,
                inputId = options.inputId || null,
                searchId = options.searchId || null,
                inputClearId = options.inputClearId || null,
                autoChange = options.autoChange || false,
                enterSearch = options.enterSearch || false;

            $(inputClearId).hide();

            if(autoChange){
                $(filterId + ' .dropdown-menu li:not(.disabled) a').unbind().click(function () {
                    var self = $(this);

                    var selText = self.data('tg-text') || self.text();
                    var searchFor = self.data('tg-search-for') || null;
                    var txtPlace = self.data('tg-sPlace-txt') || null;
                    self.parents('.dropdown-menu').find('li').removeClass('active');
                    self.parents(filterId).find('.dropdown-toggle').html($.trim(selText)+' <span class="caret"></span>');
                    self.parent('li').addClass('active');
                    if(_.isElement($(inputId)[0])){
                        var $sIp = $(inputId);
                        $sIp.data('tg-ip-search-for', searchFor);
                        $sIp.attr('placeholder', txtPlace || ('Search '+ (searchFor || '')));
                    }

                });
                console.info("T-Grid search filter auto change enabled");
            }

            function initSearchFor(){
                tableObj['params']['searchFor'] = $(inputId).data('tg-ip-search-for') || $(filterId).data('tg-init-search') || null;
                tableObj['params']['searchValue'] = $(inputId).val() || null;
                table.populate(table, tableObj, callback);
            }

            if(_.isElement($(searchId)[0])){
                $(searchId).unbind().click(function () {
                    initSearchFor();
                });
                console.info("T-Grid search initiated.");
            }

            if(_.isElement($(inputClearId)[0])){
                $(inputClearId).unbind().click(function () {
                    $(inputId).val('');
                    $(this).hide();
                    initSearchFor();
                    console.info("T-Grid input Cleared.");
                });
                console.info("T-Grid Clear initiated.");
            }

            if(enterSearch){
                $(inputId).unbind().keyup(function (e) {
                    if(!$(this).val()){ $(inputClearId).hide(); initSearchFor(); } else { $(inputClearId).show(); }
                    if(e.keyCode == 13){
                        initSearchFor();
                        console.info("T-Grid search initiated.");
                    }
                });
            }
        }

        $(table.targetId + ' li.rec').unbind().click(function () {
            var rpValue = $.trim($(this).find('a').html());
            $(table.targetId + ' .records-per-page-value').html(rpValue);
            $(table.targetId + ' .records-per-page-options > li').removeClass('active');
            $(this).addClass('active');
            table.params.offset = 0;
            table.params.limit = rpValue;
            table.populate(table, tableObj, callback);
        });


        // record per page
        $(table.targetId + ' li.rec').unbind().click(function () {
            var rpValue = $.trim($(this).find('a').html())
            $(table.targetId + ' .records-per-page-value').html(rpValue);
            $(table.targetId + ' .records-per-page-options > li').removeClass('active');
            $(this).addClass('active');
            table.params.offset = 0;
            table.params.limit = rpValue;
            table.populate(table, tableObj, callback);
        });


        // next page
        if(!table.nextDOM){ var $next = $(table.footerTarget).find('.next'); }else{ var $next = $(table.targetId + ' .next,' + table.nextDOM); }
        $next.unbind().click(function (e) {
            table.next(table, tPage, tableObj, callback)
        });


        // prev page
        if(!table.prevDOM){ var $prev = $(table.footerTarget).find('.prev'); }else{ var $prev = $(table.targetId + ' .prev,' + table.prevDOM); }
        $prev.unbind().click(function () {
            table.prev(table, tPage, tableObj, callback)
        });

        // Refresh page
        if(table.refreshId){
            var $refresh = $(table.refreshId);
            $refresh.unbind().click(function () {
                table.populate(table, tableObj, callback);
            });
        }

        //sort page
        $(table.targetId).find("[data-sort]").unbind().click(function (e) {
            var sortField = $(this).data('sort');
            var sortOrder = $(this).data('sort-order');

            table.sort(table, sortField, sortOrder, tableObj, callback)
        });

        // more link
        $(table.footerTarget).find('.tgrid-more').unbind().click(function () {
            var currentPage = (Math.round(table.params.offset / table.params.limit)+1);
            if (tPage > currentPage) { table.footerTarget.html('<center><i class="tgrid-loading-container">' + table.loading + table.loadingDots + '</i></center>'); }
            table.next(table, tPage, tableObj, callback)
        });

        //scroll end
        if(table.autoScroll && table.isMoreEnabled){
            $(window).unbind().scroll(function(){
//                if($(document).height()== $(window).scrollTop()+$(window).height()){
                if($(document).height()== $(window).scrollTop()+$(window).height()){
                    if(table.params.page_number % 11 == 0) { return false; }
                    if (tPage > table.params.page_number) { table.footerTarget.html('<center><i class="tgrid-loading-container">' + table.loading + table.loadingDots + '</i></center>'); }
                    table.next(table, tPage, tableObj, callback)
                }
            });
        }

        // go to page via input
        $(table.footerTarget).find('input.currentPageSelection').keyup(function (event) {
            if(event.keyCode == 13){
                var page = $.trim($(this).val());
                var goToPage = (page != '' && page != 0) ? page : 1;
                table.params.offset = (goToPage-1) * table.params.limit;
                table.populate(table, tableObj, callback);
            }

        });

        $(table.footerTarget).find('input.currentPageSelection').keypress(function(event){
            if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
                event.preventDefault();
            }});



        // go to page
        $(table.footerTarget).find('select.currentPageSelection').change(function () {
            var goToPage = $(this).val() * 1;
            table.params.offset = (goToPage-1) * table.params.limit;
            table.populate(table, tableObj, callback);
        });
    },
    next: function (table, tPage, tableObj, callback) {

        if (table.params.limit != null) {
            var currentPage = (Math.round(table.data.data.offset / table.data.data.limit)+1);
            if (tPage > currentPage) {
                table.params.offset = currentPage * table.params.limit;
                table.populate(table, tableObj, callback, 10);
            }
        }
    },
    prev: function (table, tPage, tableObj, callback) {
        if (table.params.limit != null) {
            var currentPage = (Math.round(table.data.data.offset / table.data.data.limit)+1);
            if (currentPage != 1) {
                table.params.offset = table.data.data.offset - table.params.limit;
                table.populate(table, tableObj, callback, 01);
            }
        }
    },
    more: function(){

    },
    sort: function (table, sortField, sortOrder, tableObj, callback) {
        var sortObjExist = _.where(table.sortByList, {
            columnName: sortField
        });
        var $gridSortDOM = $(table.targetId).find("[data-sort='" + sortField + "']");
        $gridSortDOM.removeClass('asc desc')

        if (sortObjExist.length == 0) {

            if (sortOrder == 1) {
                var sortObj = {
                    columnName: sortField,
                    order: 0
                };
                $gridSortDOM.removeClass('asc').removeClass('desc');

            } else if (sortOrder == -1) {
                var sortObj = {
                    columnName: sortField,
                    order: 1
                };
                $gridSortDOM.addClass('asc')
            } else {
                var sortObj = {
                    columnName: sortField,
                    order: -1
                };
                $gridSortDOM.addClass('desc')
            }

            if (table.sortType.toLowerCase() == 'single') {
                table.sortByList = [sortObj]
            } else {
                table.sortByList.push(sortObj)
            }

        } else {
            if (sortObjExist[0].order == 1) {
                sortObjExist[0].order = 0
                $gridSortDOM.removeClass('asc').removeClass('desc');
                var sortIndex = _.find(table.sortByList, function (num, i) {
                    if (num.columnName == sortObjExist[0].columnName) {
                        table.sortByList.splice(i, 1);
                    }
                });
            } else if (sortObjExist[0].order == -1) {
                sortObjExist[0].order = 1
                $gridSortDOM.addClass('asc')
            } else {
                sortObjExist[0].order = -1
                $gridSortDOM.addClass('desc')
            }
        }

        table.params.sort = table.sortByList;
        table.populate(table, tableObj, callback, 11);

    },
    resize : function(){
        var table = this;
        if(table.stickyHeader){
            if(jQuery().stickyTableHeaders) {
                //run plugin dependent code
                $(table.targetId + ' table').stickyTableHeaders('destroy');
                $(table.targetId + ' table').stickyTableHeaders();
            }else{
                console.error('Sticky table headers not configured properly');
            }
        }
    },
    drawDiv: function (self, tableObj, data, callback, type) {
        var table = self;

        table.isMoreEnabled = true;
        if((table.isMoreEnabled && !$(table.targetId + ' .tgrid-content').exists()) || (!table.isMoreEnabled || !$(table.targetId + ' .tgrid-content').exists())){
            //$(table.targetId).html('<div class="tgrid-content"></div>');
            $(table.targetId).html('<div class="tgrid-content"></div><div class="row"><div class="col-lg-12 tgrid-footer" style="margin-bottom: 10px; margin-top: 15px;"></div></div>');

        }

        if(!$(table.targetId + ' + div .tgrid-footer').exists()){
            //$("<div class='row'><div class='col-lg-12 tgrid-footer' style='margin-bottom: 10px; margin-top: 5px;'></div></div>" ).insertAfter($(table.targetId));
        }

        // body content main
        if (data.data.list && data.data.list.length > 0) {

            _.each(data.data.list, function (val, key) {
                $(table.targetId + ' .tgrid-content').append(_.template(table.tbody, {variable: 'data'})(val));
            });
            self.footerTarget = $(table.targetId + ' .tgrid-footer');
            this.drawPagination(self, tableObj, data, callback, type);
        } else {
            $(table.targetId + ' .tgrid-content').html('<div class="col-lg-12" style="text-align: center; height:150px; vertical-align:middle;" class="tfooter"><h3 style="color:#BBB">No data found</h3></div>');
        }

        try {

            //Added by vishnu
            $(table.targetId).trigger('tgrid-loaded',{targetId : table.targetId});

            callback(table)
        } catch (e) {}
    },
    location : function(self, tableObj, data, callback){
        var _headTmp = _tableTmp.slice(_tableTmp.search("<tgrid:head>"),_tableTmp.search("</tgrid:head>"));
        var _bodyTmp = _tableTmp.slice(_tableTmp.search("<tgrid:body>"),_tableTmp.search("</tgrid:body>"));
        self.thead = _headTmp.replace('<tgrid:head>','');
        self.tbody = _bodyTmp.replace('<tgrid:body>','');
    }
};


// isExist Plugin
jQuery.fn.exists = function(){return this.length>0;}

/**
 * Copyright 2017 Teezle LLC.
 **/