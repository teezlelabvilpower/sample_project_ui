var nProgressLoad = function(){
    NProgress.start();
}

function notEnabled(){

    $.toast({
        text: 'Not Enabled',
        bgColor: '#f2dede',
        textColor: '#b94a48',
        position: 'top-center',
        textAlign: 'center',
        hideAfter: 2000,
        stack:false,
        loader: false,
        allowToastClose: false,


    })
}