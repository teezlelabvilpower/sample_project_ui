
$(window).bind("load", function() {
    initMap();
});

var locations = [
    ['Cowra',-33.8281, 148.6779],
    ['Boorowa', -34.4393, 148.7226],
    ['Wollongong',-34.4278, 150.8931],
    ['Gundagai',-35.0566, 148.1021],
    ['ungarie',-33.6415, 146.9747],
    ['jerilderie',-35.3524, 145.7377],
    ['booligal',-33.8787, 144.8775],
    ['gilgunnia',-32.4421, 146.0719],
    ['binnaway',-31.5500, 149.3833],
    ['byrock',-30.4364, 146.5763],
    ['marree',-29.6481, 138.0630],
    ['mamungari',-28.4215, 129.4864],

    ['garah',-29.0605, 149.6258],
    ['hawker',-31.8884, 138.4246],
    ['kadina',-33.9633, 137.7149],
    ['peak hill',-32.7281, 148.1929],
    ['fowlersgap',-31.3642, 141.6788],
    // ['australia',-25.2744, 133.7751],
    ['germany',51.1657,10.4515],
    ['kenya',-0.0236,37.9062],
    ['meru',-0.0515,37.6456],
    ['newzealand',-40.9006, 174.8860],

];
var map;
var markers = [];
var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';



function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -14.5994, lng: 28.6731},
        zoom: 2
    });
    map.setOptions({ minZoom: 2 });

    var num_markers = locations.length;
    for (var i = 0; i < num_markers; i++) {
        markers[i] = new google.maps.Marker({
            position: {lat:locations[i][1], lng:locations[i][2]},
            map: map,
            html: locations[i][0],
            id: i,
        });

        google.maps.event.addListener(markers[i], 'click', function(){
            var infowindow = new google.maps.InfoWindow({
                id: this.id,
                content:this.html,
                position:this.getPosition()
            });
            google.maps.event.addListenerOnce(infowindow, 'closeclick', function(){
                markers[this.id].setVisible(true);
            });
            this.setVisible(false);
            infowindow.open(map);
        });
    }
    var markerCluster = new MarkerClusterer(map, markers,
        {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

}

