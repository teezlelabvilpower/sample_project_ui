
$(function(){

  console.log('current page');
  tzAjax(apiHost+'/v1/accountList',"get","json",function(result){
            var accountList = result;
            var accountlength = accountList.length;
            $('#selAccount').text('All Account ('+accountlength+')');
            var accList =' <li><a href="javascript:void(0)"  data-tg-search-for="all"> All</a></li>';
            for(i = 0 ; i < accountList.length ; i++){
               accList += ' <li><a href="javascript:void(0)" onClick="accountSelect(\''+accountList[i].accountName+'\')"  data-tg-search-for="'+accountList[i].accountName+'"> '+accountList[i].accountName+'</a></li>'

            }
        $('#allAccountDW').append(accList);
    })

    tzAjax(apiHost+'/v1/assetList',"get","json",function(result){
        console.log(JSON.stringify(result)+'relult');
    })

    statusTableLoad('#gridMessage');

    $("ul#allAccountDW li a").click(function(){

        $("#selAccount").html($(this).text());

    });


    $("#allTanksDW li a").click(function(){

        $("#tankFilter").html($(this).text());

    }); $("#rptStatus li a").click(function(){

        $("#rptStatusBtn").html($(this).text());

    });


    $('.rsnChkBox').click(function(){
        var val = [];
        $('.rsnChkBox:checkbox:checked').each(function(i){
            val[i] = $(this).val();
        });
        for(i=0 ; i < val.length ; i++){
            if(val[i] == 'All'){
                $(".rsnChkBox").attr("checked", true);
                $('#rsnSel').html("All");
             $("#rsnSel").html(sel);
            }else if(val[i] != "All"){
                $("#rsnSel").html(val.toString());
            }
        }

        console.log("checked"+val);
    });


   $("#ClrFilter").click(function(){

        $("#wellFilter").css({'display':'none'});

    });

   $("#statusFilterBtn").click(function(){
           if($("#wellFilter").is(':visible')){
               $("#wellFilter").css({'display':'none'});
               $('#statusFilterBtn>.icon-filter').removeClass('icon-white');
               $('#statusFilterBtn').addClass('btn-default');
               $('#statusFilterBtn').removeClass('btn-primary');
           }else{
               $('#statusFilterBtn').removeClass('btn-default');
               $('#statusFilterBtn>.icon-filter').addClass('icon-white');
               $('#statusFilterBtn').addClass('btn-primary');
               $("#wellFilter").css({'display':'block'});
           }


    });

});
function accountSelect(account){
    $("#selAccount").html(account);
}
function showdashboardphotomap(lat , lon , location , tankId){
    map = new google.maps.Map(document.getElementById('imgUploadMap'), {
        center: {lat: lat , lng: lon},
        zoom: 15
    });
    var beachMarker = new google.maps.Marker({
        position: {lat: lat, lng: lon},
        map: map,
        icon: 'images/maps/maptankicon.png'
    });
}


function statusTableLoad(sortField ){
  nProgressLoad();
    var   sortField = sortField ? sortField : '#messageId';
         var tableObj = {
        targetId: '#statusTarget',
        templateId: "#statusTemplate",
        url: apiHost+'/v1/status/report', // API call URL

        params: {
            limit: 5,
            offset: 0,
            accountIds:'ALL',
            menu:'status',
            assetType : 'tank',
            search:'',
            records_per_page:'',
            //searchFor:searchFor,
            //searchValue:searchValue,
            sort : '',
            page_number: 1 ,
            sortType:'single',
            sortField:sortField,
            sortOrder:'1',
            filter:{
            }
        },
        searchOptions : {
            enabled : true,
            filterId : "",// Dropdown Filter Id (if available)
            inputId : "#statusSearchBox", // Search Input Id ()
            inputClearId : "#inputClearBtn", // Search Clear button Id ()
            searchId : "#statusSearchId", // Search button Id
            autoChange: true, // true - Change Happens based on dropdown change
            enterSearch: true // true - If search while happens while press enter button
        },
        refreshId: "#statusRefresh",// Refresh button Id
        recperpage: [10, 25, 50,100],
        hideFooter : false,
        loadingDots:'',
        layout: 'table'
    };


    var table = new tGrid(tableObj, function (result) {
        console.log("Table Drawn");

        NProgress.done();
    });
}

function scaleUnitConversion(value, scaleUnit){

    var data = ""
    if(scaleUnit == "meters"){
        data = roundTo(value / 39.370, 2);
    }else if(scaleUnit == "feet"){
        data = roundTo(value * 0.083333, 2);
    }
    return data;
}

