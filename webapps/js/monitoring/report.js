
$(function(){
    sidemenu('rawList');
    $("table" ).css({ 'margin-bottom': "0px"});

    $("#allReportAcc li a").click(function(){
        $("#allAccToggle span:nth-child(2)").html($(this).text());
    });

    $("#dailePageId li ").click(function(){
        $("#selectIdDaily span:first-child").html($(this).text());
    });

    $("#dli").addClass('dli-active');
    // $("#datePickFrom").datepicker();
})

$(document).ready(function() {
    $("#datePickFrom").datepicker();
    $("#datePickTo").datepicker();

});

$(document).ready(function(){
    $('.side-column').height($(window).height()-30);
});


function clrfromto() {

    document.getElementById("datePickFrom").value = "";
    document.getElementById("datePickTo").value = "";
}

function clrrr() {

    document.getElementById("rawSBox").value = "";
    document.getElementById("decodeSBox").value = "";
}


function acknow(id){
    if(id=="acknow1"){
        $("#acknow1").removeClass('btn-default');
        $("#acknow1").addClass('btn-primary');
        $("#iconOkWhite").removeClass('icon-white');
        $("#iconOk").addClass('icon-white');

        $("#acknow2").removeClass('btn-primary');
        $("#acknow2").addClass('btn-default');
    }
    else if(id=="acknow2")
    {
        $("#iconOkWhite").addClass('icon-white');
        $("#iconOk").removeClass('icon-white');
        $("#acknow2").removeClass('btn-default');
        $("#acknow2").addClass('btn-primary');
        $("#acknow1").removeClass('btn-primary');
        $("#acknow1").addClass('btn-default');
    }
}

function sidemenu(id)
{
    nProgressLoad();
    if(id=="rawList")
    {
        $("#fromId").show();
        $("#srchRaw").show();
        $("#srchDecode,#srchDaily").hide();
        $('#refreshDivId').hide();
        $(".acknow-button").hide();
        $(".last-divv").show();
        $(".report-datediv").hide();
        $(".daily-datelbl").hide();

        console.log('load');
        reportTable('#reportTemplateId','#msgTimeId', '/rawMessage');

        $(".clr-searchdiv").show();
        $(".clr-searchdiv .btn-min").css({'width':'unset','height':'unset','font-size':'11px'});
        $("#rawList").addClass('report-menu-active');

        $("#decodeList,#alrmList,#dailyReportList").removeClass('report-menu-active');
    }

    else if(id=="decodeList")
    {
        $("#srchRaw,#srchDaily").hide();
        $("#refreshDivId").hide();
        $("#srchDecode").show();
        $("#fromId").hide();
        $(".clr-searchdiv").show();
        $(".clr-searchdiv").css({'margin-top':'7px'});
        $(".clr-searchdiv .btn-min").css({'width':'90px','height':'26px','font-size':'13px'});
        $(".acknow-button").hide();
        $(".last-divv").show();
        $(".report-datediv").hide();

        $(".daily-datelbl").hide();

        reportTable('#reportTemplateId','#msgTimeId', '/decodeMessage');

        $("#decodeList").addClass('report-menu-active');
        $("#alrmList,#rawList,#dailyReportList").removeClass('report-menu-active');
    }
    else if(id=="alrmList")
    {
        $("#refreshDivId").show();
        $("#srchRaw,#srchDecode,#srchDaily").hide();
        $("#fromId").hide();
        $(".clr-searchdiv").hide();
        $(".acknow-button").show();
        $(".last-divv").show();
        $(".report-datediv").hide();
        $(".daily-datelbl").hide();

        reportTable('#reportAlarm','#msgTimeId', '/alarmMsg');

        $("#alrmList").addClass('report-menu-active');
        $("#decodeList,#rawList,#dailyReportList").removeClass('report-menu-active');
    }
    else if(id=="dailyReportList")
    {
        $(".clr-searchdiv").hide();
        $("#srchRaw,#srchDecode").hide();
        $("#srchDaily").show();
        $("#fromId").hide();
        $(".acknow-button").hide();
        $("#refreshDivId").show();
        $(".last-divv").hide();
        $(".daily-datelbl").show();
        $(".report-datediv").show();
        reportTable('#reportDaily','#msgTimeId', '/dailyReport');

        $("#dailyReportList").addClass('report-menu-active');
        $("#decodeList,#rawList,#alrmList").removeClass('report-menu-active');
    }
}

// function sortField(id){
//     var sortingField = '#'+id ;
//     reportTable(sortingField);
// }

function reportTable(templateId ,  sortField , api){

    console.log(api);
    var   sortField = sortField ? sortField : ''



    var tableObj = {
        targetId: '#reportTargetId',
        templateId: templateId,
        url: apiHost+api, // API call URL
        params: {
            limit: 10,
            offset: 0,
            //searchFor:searchFor,
            //searchValue:searchValue,
            sort : '',
            sortType:'single',
            sortField:sortField,
            sortOrder:'',
            filter:{
            }
        },
        searchOptions : {
            enabled : true,
            filterId : "#filterId",// Dropdown Filter Id (if available)
            inputId : "#searchBox", // Search Input Id ()
            inputClearId : "#inputClearBtn", // Search Clear button Id ()
            searchId : "#searchId", // Search button Id
            autoChange: true, // true - Change Happens based on dropdown change
            enterSearch: true // true - If search while happens while press enter button
        },
        refreshId: "#reportRefresh",// Refresh button Id
        recperpage: [10, 25, 50,100],
        hideFooter : false,
        loadingDots:'',
        layout: 'table'
    };


    var table = new tGrid(tableObj, function (result) {
        console.log("Table Drawn");
        if(api == '/rawMessage'){
            $("#reportMessageId").html('Raw Message');
        }else if(api == '/decodeMessage'){
            $("#reportMessageId").html('Decode Message');
        }

        
        NProgress.done();
    });
}


/*$(".dropdown-menu>li>a").click(function() {
    alert('click');
    console.log('click');
    $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
    $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
});*/

