
$(function () {
    loadMngAccounts();
})

function loadMngAccounts() {

    var tableObj = {
        targetId: '#manageAccAccounts',
        templateId: "#mngActTemplateId",
        url: apiHost+'/snapshot/history-grid', // API call URL
//                                                    url: 'http://tankiq.tmatics.com/asset/assetstatushistory?asset_id=58ec83be71074fbc4d000002&page_number=1&records_per_page=10&sort%5B0%5D%5BcolumnName%5D=messageTime&sort%5B0%5D%5Border%5D=-1&fromDate=&toDate=&dateFilterField=messageTimeMillis', // API call URL
        params: {
            limit: 10,
            offset: 0,
//searchFor:searchFor,
//searchValue:searchValue,
            sort: '',
            sortType: 'single',
            sortField: '',
            sortOrder: '',
            filter: {}
        },
        searchOptions: {
            enabled: true,
            filterId: "#filterId",// Dropdown Filter Id (if available)
            inputId: "#searchBox", // Search Input Id ()
            inputClearId: "#inputClearBtn", // Search Clear button Id ()
            searchId: "#searchId", // Search button Id
            autoChange: true, // true - Change Happens based on dropdown change
            enterSearch: true // true - If search while happens while press enter button
        },
        refreshId: "#refreshId",// Refresh button Id
        recperpage: [10, 25, 50, 100],
        hideFooter: true,
        loadingDots: '',
        layout: 'table'
    };


    var table = new tGrid(tableObj, function (result) {
        console.log("Table Drawn");


        NProgress.done();

    });
}