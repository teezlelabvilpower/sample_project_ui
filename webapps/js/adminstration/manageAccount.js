
// function assetManageAccount(){
//     $("#asset").css({"background-color":"#0088CC"})
// }

$(function(){

    $("#manageAll").css({'border-bottom':'2px solid #0088CC','border-top':'2px solid #0088CC',
        'background-color':'white'});

//        var interest =  $("ul#hi>li.active").attr("id");

    $("#tankPageUl,#oldTank").show();
    $("#devicePageUl,#userPageUl,#accountPageUl,#newTank").hide();
})


function sidefunction(id){
    if(id=="tankyId"){
        $("#tankPageUl,#oldTank").show();
        $("#devicePageUl,#userPageUl,#accountPageUl,#newTank").hide();
//            $("#uploadTank")
    }

    else if(id=="deviceeId"){
        $("#tankPageUl,#userPageUl,#accountPageUl").hide();
        $("#devicePageUl").show();

        $("#manageAllDev").css({'border-bottom':'2px solid #0088CC','border-top':'2px solid #0088CC',
            'background-color':'white'});
        $("#addTankDev").css({'border':'0px','background-color':'transparent'});
    }

    else if(id=="userssId"){
        $("#tankPageUl,#devicePageUl,#accountPageUl,#newUser,#roleeId").hide();
        $("#userPageUl,#oldUser").show();

        $("#manageAllUser").css({'border-bottom':'2px solid #0088CC','border-top':'2px solid #0088CC',
            'background-color':'white'});
        $("#addTankUser,#roleTankUser").css({'border':'0px','background-color':'transparent'});
    }

    else if(id=="accountssId"){
        $("#tankPageUl,#devicePageUl,#userPageUl").hide();
        $("#accountPageUl").show();

        $("#manageAllAcc").css({'border-bottom':'2px solid #0088CC','border-top':'2px solid #0088CC',
            'background-color':'white'});
        $("#addTankAcc").css({'border':'0px','background-color':'transparent'});
    }
}

function addtank(id){
    if(id=="addTank"){
        $("#addTank").css({'border-bottom':'2px solid #0088CC','border-top':'2px solid #0088CC',
            'background-color':'white'});
        $("#manageAll").css({'border':'0px','background-color':'transparent'});

        $("#newTank").show();
        $("#oldTank").hide();

    }

    else if(id=="manageAll"){
        $("#manageAll").css({'border-bottom':'2px solid #0088CC','border-top':'2px solid #0088CC',
            'background-color':'white'});
        $("#addTank").css({'border':'0px','background-color':'transparent'});

        $("#newTank").hide();
        $("#oldTank").show();
    }
}


function secondfunction(id){
    if(id=="addTankDev"){
        $("#addTankDev").css({'border-bottom':'2px solid #0088CC','border-top':'2px solid #0088CC',
            'background-color':'white'});
        $("#manageAllDev").css({'border':'0px','background-color':'transparent'});

    }

    else if(id=="manageAllDev"){
        $("#manageAllDev").css({'border-bottom':'2px solid #0088CC','border-top':'2px solid #0088CC',
            'background-color':'white'});
        $("#addTankDev").css({'border':'0px','background-color':'transparent'});
    }
}


function thirdfunction(id){
    if(id=="addTankUser"){
        $("#addTankUser").css({'border-bottom':'2px solid #0088CC','border-top':'2px solid #0088CC',
            'background-color':'white'});
        $("#manageAllUser,#roleTankUser").css({'border':'0px','background-color':'transparent'});

        $("#newUser").show();
        $("#oldUser,#roleeId,#roleButton").hide();
    }

    else if(id=="manageAllUser"){
        $("#manageAllUser").css({'border-bottom':'2px solid #0088CC','border-top':'2px solid #0088CC',
            'background-color':'white'});
        $("#addTankUser,#roleTankUser").css({'border':'0px','background-color':'transparent'});

        $("#newUser,#roleeId,#roleButton").hide();
        $("#oldUser").show();
    }

    else if(id=="roleTankUser"){
        $("#roleTankUser").css({'border-bottom':'2px solid #0088CC','border-top':'2px solid #0088CC',
            'background-color':'white'});
        $("#addTankUser,#manageAllUser").css({'border':'0px','background-color':'transparent'});

        $("#newUser,#oldUser,#roleButton").hide();
        $("#roleeId").show();
    }
}


function fourthfunction(id){
    if(id=="addTankAcc"){
        $("#addTankAcc").css({'border-bottom':'2px solid #0088CC','border-top':'2px solid #0088CC',
            'background-color':'white'});
        $("#manageAllAcc").css({'border':'0px','background-color':'transparent'});
    }

    else if(id=="manageAllAcc"){
        $("#manageAllAcc").css({'border-bottom':'2px solid #0088CC','border-top':'2px solid #0088CC',
            'background-color':'white'});
        $("#addTankAcc").css({'border':'0px','background-color':'transparent'});
    }
}

