
$(function(){

    var tableObj = {
        targetId: '#tankTargetId',
        templateId: '#tankTemplateId',
       url: apiHost+'/manageAccount/manageTankData', /// API call URL
        params: {
            limit: 12,
            offset: 0,
            //searchFor:searchFor,
            //searchValue:searchValue,
            sort : '',
            sortType:'single',
            sortField:'',
            sortOrder:'',
            filter:{
            }
        },
        searchOptions : {
            enabled : true,
            filterId : "#filterId",// Dropdown Filter Id (if available)
            inputId : "#searchBox", // Search Input Id ()
            inputClearId : "#inputClearBtn", // Search Clear button Id ()
            searchId : "#searchId", // Search button Id
            autoChange: true, // true - Change Happens based on dropdown change
            enterSearch: true // true - If search while happens while press enter button
        },
        refreshId: "#refreshId",// Refresh button Id
        hideFooter : false,
        recperpage: [10, 25, 50,100],
        loadingDots:'',
        layout: 'table'
    };
    var table = new tGrid(tableObj, function (result) {
        console.log("Table Drawn");
    });
});
