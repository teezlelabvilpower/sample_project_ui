
$(function(){



    loadUserData();


    function loadUserData(){
        nProgressLoad();
        var tableObj = {
            targetId: '#manageUser',
            templateId: "#userTemplate",
            url: 'manageAccount/manageUserData', // API call URL
            params: {
                limit: 10,
                offset: 0,
                //searchFor:searchFor,
                //searchValue:searchValue,
                sortField:'',
                sortOrder:'',
                filter:{
                }
            },
            searchOptions : {
                enabled : true,
                filterId : "#filterId",// Dropdown Filter Id (if available)
                inputId : "#searchBox", // Search Input Id ()
                inputClearId : "#inputClearBtn", // Search Clear button Id ()
                searchId : "#searchId", // Search button Id
                autoChange: true, // true - Change Happens based on dropdown change
                enterSearch: true // true - If search while happens while press enter button
            },
            refreshId: "#refreshId",// Refresh button Id
            recperpage: [10, 25, 50,100],
            hideFooter : false,
            layout: 'table'
        };


        var table = new tGrid(tableObj, function (result) {
            console.log("Table Drawn");

            NProgress.done();
        });
    }

    loadRoleData();


    function loadRoleData(){
        nProgressLoad();
        var tableObj = {
            targetId: '#manageRole',
            templateId: "#roleTemplate",
            url: 'manageAccount/manageUserData', // API call URL
            params: {
                limit: 5,
                offset: 0,
                //searchFor:searchFor,
                //searchValue:searchValue,
                sortField:'',
                sortOrder:'',
                filter:{
                }
            },
            searchOptions : {
                enabled : true,
                filterId : "#filterId",// Dropdown Filter Id (if available)
                inputId : "#searchBox", // Search Input Id ()
                inputClearId : "#inputClearBtn", // Search Clear button Id ()
                searchId : "#searchId", // Search button Id
                autoChange: true, // true - Change Happens based on dropdown change
                enterSearch: true // true - If search while happens while press enter button
            },
            refreshId: "#refreshId",// Refresh button Id
            recperpage: [10, 25, 50,100],
            hideFooter : false,
            layout: 'table'
        };
     var table = new tGrid(tableObj, function (result) {
            console.log("Table Drawn");

            NProgress.done();
        });
    }
});

function createuser() {
    // alert("hi");


    if (document.getElementById('manageUser')) {

        if (document.getElementById('oldUser').style.display == 'none') {
            document.getElementById('oldUser').style.display = 'block';
            document.getElementById('newUser').style.display = 'none';
        }
        else {
            document.getElementById('oldUser').style.display = 'none';
            document.getElementById('newUser').style.display = 'block';
        }
    }

    $("#addTankUser").css({'border-bottom':'2px solid #0088CC','border-top':'2px solid #0088CC',
        'background-color':'white'});
    $("#manageAllUser").css({'border':'0px','background-color':'transparent'});

}


function createrole(id){
    $("#roleeId").hide();
    $("#roleButton").show();
}

function addnew() {
    // alert("hi");
    $("#newUser").hide();
    $("#roleButton").show();
}

function managerole() {
    // alert("ho");
    $("#roleButton").hide();
    $("#roleeId").show();

}
