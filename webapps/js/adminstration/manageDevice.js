$(function(){
    tgridLoad();

    $("#devType li a").click(function(){

        $("#sDvcType").html($(this).text());

    });
    $("#dVolt li a").click(function(){

        $("#sVolt").html($(this).text());

    });
})

function tgridLoad( ){
    nProgressLoad();

    var tableObj = {
        targetId: '#mngDevice',
        templateId: "#mngDvcTemplate",
        url: 'v1/status/report', // API call URL

        params: {
            limit: 10,
            offset: 0,
            accountIds:'ALL',
            menu:'status',
            assetType : 'tank',
            search:'',
            records_per_page:'',
            //searchFor:searchFor,
            //searchValue:searchValue,
            sort : '',
            page_number: 1 ,
            sortType:'single',
            sortField:'',
            sortOrder:'1',
            filter:{
            }
        },
        searchOptions : {
            enabled : true,
            filterId : "",// Dropdown Filter Id (if available)
            inputId : "#dvcSrch", // Search Input Id ()
            inputClearId : "#deviceClear", // Search Clear button Id ()
            searchId : "#dvcSrchBtn", // Search button Id
            autoChange: true, // true - Change Happens based on dropdown change
            enterSearch: true // true - If search while happens while press enter button
        },
        refreshId: "",// Refresh button Id
        recperpage: [10, 25, 50,100],
        hideFooter : false,
        loadingDots:'',
        layout: 'table'
    };


    var table = new tGrid(tableObj, function (result) {
        console.log("Table Drawn");

        NProgress.done();
    });
}
