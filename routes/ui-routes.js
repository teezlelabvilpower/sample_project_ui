
var uiAction = require('../controller/ui-action.js');
var _ = require('underscore')


var UIRoutes = function (app) {
    this.app = app;
    this.conf=app.conf;
    this.actionInstance = new uiAction(app);
};

module.exports = UIRoutes;

UIRoutes.prototype.init = function () {

    var app = this.app;
    var conf = this.conf;
    var isAuthorized = app.sessionCheck;
    var actionInstance = this.actionInstance;

    /*-------------------------- UI Routes ----------------------------*/

    //Init Routes
    app.get("/", function (req, res) {
        actionInstance.getLoginPage(req, res);
    });
    app.get("/login", function (req, res) {
        actionInstance.getLoginPage(req, res);
    })
    ;app.get("/loginSubmit", function (req, res) {
        actionInstance.getLoginSubmitPage(req, res);
    });
     app.get("/status", function (req, res) {
        actionInstance.getStatusPage(req, res);
    });
      app.get("/snapshots", function (req, res) {
        actionInstance.getSnapshotPage(req, res);
    });
      app.get("/mapping", function (req, res) {
        actionInstance.getMappingPage(req, res);
    });
      app.get("/reports", function (req, res) {
        actionInstance.getReportPage(req, res);
    });
      app.get("/notifications", function (req, res) {
        actionInstance.getNotifyPage(req, res);
    });

    app.get("/admin", function (req, res) {
        actionInstance.getMyAccountPage(req, res);
    });
   app.get("/manageAccount", function (req, res) {
        actionInstance.getManageAccountPage(req, res);
    });
    app.get("/manageUser", function (req, res) {
        actionInstance.getManageUserPage(req, res);
    });
    app.get("/manageAccountMenu", function (req, res) {
        actionInstance.getManageAccountMenuPage(req, res);
    });
    app.get("/manageTank", function (req, res) {
        actionInstance.getManageTankPage(req, res);
    });

  app.get("/managedevice", function (req, res) {
        actionInstance.getmanageDevice(req, res);
    });

 app.get("/checking", function (req, res) {
        actionInstance.getmyaccount(req, res);
    });



    //SAMPLE GRID PAGE


    app.get("/v1/status/report", function (req, res) {
        actionInstance.getStatusGridData(req, res);
    });
    app.get("/v1/accountList", function (req, res) {
        actionInstance.getaccountListData(req, res);
    });
    app.get("/v1/assetList", function (req, res) {
        actionInstance.gettankListData(req, res);
    });

 app.get("/rawMessage", function (req, res) {
        actionInstance.getrawMessageData(req, res);
    });
 app.get("/decodeMessage", function (req, res) {
        actionInstance.getdecodeData(req, res);
    });
    app.get("/alarmMsg", function (req, res) {
        actionInstance.getalarmData(req, res);
    });
    app.get("/dailyReport", function (req, res) {
        actionInstance.getdailyData(req, res);
    });

    app.get("/snapshot/history-grid", function (req, res) {
        actionInstance.getHistoryData(req, res);
    });

    app.get("/manageAccount/manageTankData", function (req, res) {
        actionInstance.getManageTankData(req, res);
    });
    app.get("/manageAccount/manageUserData", function (req, res) {
        actionInstance.getManageUserData(req, res);
    });
    app.get("/mapping/mapTable", function (req, res) {
        actionInstance.getMapTable(req, res);
    });
}