/**
 * Created by thilagam on 28/9/15.
 */
var conf = {
    web: {
        host: "0.0.0.0",
        port: "9000",
        method: "session",
        views: {},
        static: {}
    },
    database: {
        api: 'mongodb',
        host: '10.1.10.47',
        port: '12345',
        schema: 'teezle_web',
        auth: false,
        username: '',
        password: ''
    },

    session: {
        store: 'inmemory', // redis | mongo | inmemory //TODO mongo not implemented
        host: '10.1.10.47',
        port: '6379'
    },
    mapKey :"AIzaSyD2iBxqvigy_WDahAwkKYgB_GEgq8AO0CY",
    apiHost :{
        api : "43.225.166.236:9000",

    }
};

module.exports = conf;
